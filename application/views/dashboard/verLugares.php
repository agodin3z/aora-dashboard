<div class="page-title">
  <div class="title_left">
    <h3>Datos Registrados</h3>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Lugares <small>(Todos los registros)</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <!--<li class="pull-right"><a class="close-link"><i class="fa fa-close"></i></a>-->
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table class="table table-bordered table-responsive">
          <thead>
            <tr>
              <th>Nombre</th>
              <!--<th>Imagen</th>-->
              <th>Twitter</th>
              <!--<th>Descripcion</th>-->
              <th>Email</th>
              <th>Latitud</th>
              <th>Longitud</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $consulta = $this->db->get('lugar');
              foreach ($consulta ->result() as $row){
                echo "<tr><td>" . $row->name . "</td><td>" . $row->twitter . "</td><td>" . $row->email . "</td><td>" . $row->lat . "</td><td>" . $row->lng . "</td><td><a href='" . base_url('Dashboard/change_place/'.$row->id) . "' class='btn btn-warning'>Editar</a>
            <a href='" . base_url('Dashboard/erase_place/'.$row->id) . "' class='btn btn-danger'>Borrar</a></td><td></td></tr>" ;
              }
            ?>
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>