<?php
/*if(@$notif = 'success'){
  ?>
  <script>
    swal({
      position: 'top-right',
      type: 'success',
      title: 'Registro Guardado!',
      showConfirmButton: false,
      timer: 1500
    })
  </script>
  <?php
}
if(@$notif = 'danger'){
  ?>
  <script>
    swal('Oops...','Datos incorrectos!','error')
  </script>
  <?php
}*/
?>
<div class="page-title">
  <div class="title_left">
    <h3>Formulario de Registro</h3> <?php echo @$notif; ?>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Registro de Eventos</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <!--<li><a class="close-link"><i class="fa fa-close"></i></a>-->
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <br />
      <form class="form-horizontal form-label-left" method="post" action="<?= base_url('Dashboard/register_event'); ?>">
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre: <span class="required">*</span></label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="Nombre del evento..." name="name" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">URL Imagen: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="Nombre del evento..." name="eventImg">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Lugar: <span class="required">*</span></label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <select class="select2_single form-control" tabindex="-1" name="speakerNames" required>
              <?php 
              $consulta = $this->db->get('lugar');
              foreach ($consulta ->result() as $row){
                echo "<option value='". $row->name . "'>" . $row->name . "</option>";
              }
            ?>

            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha: <span class="required">*</span></label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="date" class="form-control" placeholder="1/1/17" name="fecha" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Hora de Inicio: <span class="required">*</span></label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="time" class="form-control" placeholder="00:00" name="timeStart" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Hora de Fin: <span class="required">*</span></label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="time" class="form-control" placeholder="00:00" name="timeEnd" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción: <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <textarea class="form-control" rows="3" placeholder="Descripcion del evento..." name="description" required></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Link del Evento: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="http://evento" name="linkEvento">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Tags <span class="required">*</span></label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <select class="select2_single form-control" tabindex="-1" name="tracks" required>
              <option></option>
              <option value="Cultural">Cultural</option>
              <option value="Deportivo">Deportivo</option>
              <option value="Moda">Moda</option>
              <option value="Artístico">Artístico</option>
              <option value="Político">Político</option>
              <option value="Educativo">Educativo</option>
              <option value="Religión">Religión</option>
              <option value="Informática">Informática</option>
              <option value="Ocio">Ocio</option>
            </select>
          </div>
        </div>
        
        
        <div class="form-group">
          <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
            <input type="reset" class="btn btn-primary" value="Limpiar">
            <input type="submit" class="btn btn-success" value="Guardar">
          </div>
        </div>

      </form>
    </div>
  </div>
</div>
</div>