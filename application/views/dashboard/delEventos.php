<?php
/*if(@$notif = 'success'){
  ?>
  <script>
    swal({
      position: 'top-right',
      type: 'success',
      title: 'Registro Guardado!',
      showConfirmButton: false,
      timer: 1500
    })
  </script>
  <?php
}
if(@$notif = 'danger'){
  ?>
  <script>
    swal('Oops...','Datos incorrectos!','error')
  </script>
  <?php
}*/
?>
<div class="page-title">
  <div class="title_left">
    <h3>Formulario de Mantenimiento</h3>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>¿Realmente desea borrar el evento?</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <!--<li><a class="close-link"><i class="fa fa-close"></i></a>-->
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <br />
      <form class="form-horizontal form-label-left" method="post" action="<?= base_url('Dashboard/delete_event'); ?>">
        <?php 
          $this->db->select('*');
          $this->db->from('evento');
          $this->db->where('id',$_SESSION['id']);
          $consulta = $this->db->get()->result();
        ?>
        <!--  -->
      
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">ID: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->id) ?>" name="id" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->name) ?>" name="name" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">URL Imagen: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->eventImg) ?>" name="eventImg" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Lugar: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->speakerNames) ?>" name="speakerNames" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="date" class="form-control" value="<?php print_r($consulta[0]->fecha) ?>" name="fecha" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Hora de Inicio: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="time" class="form-control" value="<?php print_r($consulta[0]->timeStart) ?>" name="timeStart" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Hora de Fin: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="time" class="form-control" value="<?php print_r($consulta[0]->timeEnd) ?>" name="timeEnd" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción: <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <textarea class="form-control" rows="3" name="description" readonly><?php print_r($consulta[0]->description) ?></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Link del Evento: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->linkEvento) ?>" name="linkEvento" readonly>
          </div>
        </div>
        <div class="control-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Tags</label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input id="tags_1" type="text" class="tags form-control" value="<?php print_r($consulta[0]->tracks) ?>" name="tracks" />
            <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
          </div>
        </div>
        
        
        <div class="form-group">
          <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
            <input type="submit" class="btn btn-danger" value="SI, Borrar">
            <a href="<?= base_url('Dashboard/change_event/'.$_SESSION['id']) ?>" class="btn btn-warning">Modificar</a>
          </div>
        </div>

      </form>
    </div>
  </div>
</div>
</div>