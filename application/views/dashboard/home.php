<div class="page-title">
  <div class="title_left">
    <h1>Bienvenido, <?= $this->session->userdata('user_name'); ?></h1>
  </div>
</div>
<div class="clearfix"></div>

<div class="row tile_count">
  <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-calendar"></i> Eventos Registrados</span>
    <?php 
    $this->db->select('count(id) as "total"');
    $this->db->from('evento');
    $this->db->group_by('id');
    $consulta = $this->db->get()->result();
    ?>
    <div class="count"><?php print_r(sizeof($consulta));?></div>
  </div>
  <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-map-marker"></i> Lugares registrados</span>
    <?php 
    $this->db->select('count(id) as "total"');
    $this->db->from('lugar');
    $this->db->group_by('id');
    $consulta = $this->db->get()->result();
    ?>
    <div class="count"><?php print_r(sizeof($consulta));?></div>
  </div>
  <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-calendar-check-o"></i> Eventos Activos</span>
    <?php 
    $this->db->select('count(id) as "total"');
    $this->db->from('evento');
    $this->db->where('fecha >= ',date("Y-m-d", time()));
    $this->db->group_by('id');
    $consulta = $this->db->get()->result();
    ?>
    <div class="count"><?php print_r(sizeof($consulta));?></div>
  </div>
  <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-calendar-o"></i> Eventos Pasados</span>
    <?php 
    $this->db->select('count(id) as "total"');
    $this->db->from('evento');
    $this->db->where('fecha < ',date("Y-m-d", time()));
    $this->db->group_by('id');
    $consulta = $this->db->get()->result();
    ?>
    <div class="count"><?php print_r(sizeof($consulta));?></div>
  </div>
  <!--<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total Females</span>
    <div class="count">4,567</div>
    <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total Collections</span>
    <div class="count">2,315</div>
    <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total Connections</span>
    <div class="count">7,325</div>
    <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
  </div>-->
</div>
<div class="row">
  <div class="col-md-6 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Eventos <small>(Todos los registros)</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <!--<li class="pull-right"><a class="close-link"><i class="fa fa-close"></i></a>-->
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table class="table">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Lugar</th>
              <th>Fecha</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $i=1;
              $this->db->select('*');
              $this->db->from('evento');
              $this->db->order_by('fecha');
              $consulta = $this->db->get();
              foreach ($consulta ->result() as $row){
                echo "<tr><td>" . $i . "</td><td>" . $row->name . "</td><td>" . $row->speakerNames . "</td><td>" . $row->fecha . "</td></tr>" ;
                $i++;
              }
            ?>
          </tbody>
        </table>

      </div>
    </div>
  </div>

  <div class="col-md-6 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Lugares <small>(Todos los registros)</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <!--<li class="pull-right"><a class="close-link"><i class="fa fa-close"></i></a>-->
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table class="table table-responsive">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <!--<th>Imagen</th>-->
              <th>Email</th>
              <!--<th>Descripcion</th>-->
              <th>Ubicación</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $j=1;
              $consulta = $this->db->get('lugar');
              foreach ($consulta ->result() as $row){
                echo "<tr><td>" . $j . "</td><td>" . $row->name . "</td><td>" . $row->email . "</td><td>" . $row->depto . "</td></tr>" ;
              $j++;
              }
            ?>
          </tbody>
        </table>

      </div>
    </div>
  </div>

</div>