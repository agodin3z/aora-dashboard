<div class="page-title">
  <div class="title_left">
    <h3>Datos Registrados</h3>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Eventos <small>(Todos los activos)</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <!--<li class="pull-right"><a class="close-link"><i class="fa fa-close"></i></a>-->
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table class="table">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Lugar</th>
              <th>Fecha</th>
              <th>Hora Inicio</th>
              <th>Hora Fin</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $this->db->select('*');
              $this->db->from('evento');
              $this->db->where('fecha >= ',date("Y-m-d", time()));
              $this->db->order_by('fecha');
              $consulta = $this->db->get();
              foreach ($consulta ->result() as $row){
                echo "<tr><td>" . $row->name . "</td><td>" . $row->speakerNames . "</td><td>" . $row->fecha . "</td><td>" . $row->timeStart . "</td><td>" . $row->timeEnd . "</td><td><a href='" . base_url('Dashboard/change_event/'.$row->id) . "' class='btn btn-warning'>Editar</a>
            <a href='" . base_url('Dashboard/erase_event/'.$row->id) . "' class='btn btn-danger'>Borrar</a></td><td></td></tr>" ;
              }
            ?>
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Eventos Anteriores<small>(Eventos Pasados)</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <!--<li class="pull-right"><a class="close-link"><i class="fa fa-close"></i></a>-->
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table class="table">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Lugar</th>
              <th>Fecha</th>
              <th>Hora Inicio</th>
              <th>Hora Fin</th>
              <th>Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $this->db->select('*');
              $this->db->from('evento');
              $this->db->where('fecha < ',date("Y-m-d", time()));
              $this->db->order_by('fecha');
              $consulta = $this->db->get();
              foreach ($consulta ->result() as $row){
                echo "<tr><td>" . $row->name . "</td><td>" . $row->speakerNames . "</td><td>" . $row->fecha . "</td><td>" . $row->timeStart . "</td><td>" . $row->timeEnd . "</td><td><a href='" . base_url('Dashboard/change_event/'.$row->id) . "' class='btn btn-warning'>Editar</a>
            <a href='" . base_url('Dashboard/erase_event/'.$row->id) . "' class='btn btn-danger'>Borrar</a></td><td></td></tr>" ;
              }
            ?>
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>