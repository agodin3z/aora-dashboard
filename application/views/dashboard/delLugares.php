<?php
/*if(@$notif = 'success'){
  ?>
  <script>
    swal({
      position: 'top-right',
      type: 'success',
      title: 'Registro Guardado!',
      showConfirmButton: false,
      timer: 1500
    })
  </script>
  <?php
}
if(@$notif = 'danger'){
  ?>
  <script>
    swal('Oops...','Datos incorrectos!','error')
  </script>
  <?php
}*/
?>
<div class="page-title">
  <div class="title_left">
    <h3>Formulario de Mantenimiento</h3>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>¿Realmente desea borrar el evento?</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <!--<li><a class="close-link"><i class="fa fa-close"></i></a>-->
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <br />
      <form class="form-horizontal form-label-left" method="post" action="<?= base_url('Dashboard/delete_place'); ?>">
        <?php 
          $this->db->select('*');
          $this->db->from('lugar');
          $this->db->where('id',$_SESSION['id']);
          $consulta = $this->db->get()->result();
        ?>
        <!--  -->
      
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">ID: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->id) ?>" name="id" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->name) ?>" name="name" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">URL Imagen: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->profilePic) ?>" name="profilePic" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Twitter: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->twitter) ?>" name="twitter" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción: <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <textarea class="form-control" rows="3" name="about" readonly><?php print_r($consulta[0]->about) ?></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Email: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="email" class="form-control" value="<?php print_r($consulta[0]->email) ?>" name="email" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Num. Telefono: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->phone) ?>" name="phone" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Dirección: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->location) ?>" name="location" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Departamento: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->depto) ?>" name="depto" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Latitud: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="number" class="form-control" value="<?php print_r($consulta[0]->lat) ?>" name="lat" step=".0000001" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Longitud: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="number" class="form-control" value="<?php print_r($consulta[0]->lng) ?>" name="lng" step=".0000001" readonly>
          </div>
        </div>        
        <div class="form-group">
          <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
            <input type="submit" class="btn btn-danger" value="SI, Borrar">
            <a href="<?= base_url('Dashboard/change_place/'.$_SESSION['id']) ?>" class="btn btn-warning">Modificar</a>
          </div>
        </div>

      </form>
    </div>
  </div>
</div>
</div>