<?php
/*if(@$notif = 'success'){
  ?>
  <script>
    swal({
      position: 'top-right',
      type: 'success',
      title: 'Registro Guardado!',
      showConfirmButton: false,
      timer: 1500
    })
  </script>
  <?php
}
if(@$notif = 'danger'){
  ?>
  <script>
    swal('Oops...','Datos incorrectos!','error')
  </script>
  <?php
}*/
?>
<div class="page-title">
  <div class="title_left">
    <h3>Formulario de Mantenimiento</h3>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Modificación de Establecimientos</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <!--<li><a class="close-link"><i class="fa fa-close"></i></a>-->
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <br />
      <form class="form-horizontal form-label-left" method="post" action="<?= base_url('Dashboard/update_place'); ?>">
        <?php 
          $this->db->select('*');
          $this->db->from('lugar');
          $this->db->where('id',$_SESSION['id']);
          $consulta = $this->db->get()->result();
        ?>
        <!--  -->
      
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">ID: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->id) ?>" name="id" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="<?php print_r($consulta[0]->name) ?>" name="name">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">URL Imagen: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="<?php print_r($consulta[0]->profilePic) ?>" name="profilePic">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Twitter: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="<?php print_r($consulta[0]->twitter) ?>" name="twitter">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción: <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <textarea class="form-control" rows="3" name="about"><?php print_r($consulta[0]->about) ?></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Email: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="email" class="form-control" placeholder="<?php print_r($consulta[0]->email) ?>" name="email">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Num. Telefono: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="<?php print_r($consulta[0]->phone) ?>" name="phone">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Dirección: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="<?php print_r($consulta[0]->location) ?>" name="location">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Departamento: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <select class="select2_single form-control" tabindex="-1" name="depto">
              <?php echo "<option value='". $consulta[0]->depto . "'>" . $consulta[0]->depto . "</option>"; ?>
              <option value="Ahuachapán">Ahuachapán</option>
              <option value="Santa Ana">Santa Ana</option>
              <option value="Sonsonate">Sonsonate</option>
              <option value="La Libertad">La Libertad</option>
              <option value="Chalatenando">Chalatenando</option>
              <option value="San Salvador">San Salvador</option>
              <option value="Cuzcatlán">Cuzcatlán</option>
              <option value="La Paz">La Paz</option>
              <option value="Cabañas">Cabañas</option>
              <option value="San Vicente">San Vicente</option>
              <option value="Usulután">Usulután</option>
              <option value="San Miguel">San Miguel</option>
              <option value="Morazán">Morazán</option>
              <option value="La Unión">La Unión</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Latitud: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="number" class="form-control" placeholder="<?php print_r($consulta[0]->lat) ?>" name="lat" step=".0000001">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Longitud: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="number" class="form-control" placeholder="<?php print_r($consulta[0]->lng) ?>" name="lng" step=".0000001">
          </div>
        </div>        
        <div class="form-group">
          <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
            <input type="reset" class="btn btn-primary" value="Limpiar">
            <input type="submit" class="btn btn-success" value="Guardar">
          </div>
        </div>

      </form>
    </div>
  </div>
</div>
</div>