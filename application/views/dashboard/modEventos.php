<?php
/*if(@$notif = 'success'){
  ?>
  <script>
    swal({
      position: 'top-right',
      type: 'success',
      title: 'Registro Guardado!',
      showConfirmButton: false,
      timer: 1500
    })
  </script>
  <?php
}
if(@$notif = 'danger'){
  ?>
  <script>
    swal('Oops...','Datos incorrectos!','error')
  </script>
  <?php
}*/
?>
<div class="page-title">
  <div class="title_left">
    <h3>Formulario de Mantenimiento</h3>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Modificación de Eventos</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li class="pull-right"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <!--<li><a class="close-link"><i class="fa fa-close"></i></a>-->
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <br />
      <form class="form-horizontal form-label-left" method="post" action="<?= base_url('Dashboard/update_event'); ?>">
        <?php 
          $this->db->select('*');
          $this->db->from('evento');
          $this->db->where('id',$_SESSION['id']);
          $consulta = $this->db->get()->result();
        ?>
        <!--  -->
      
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">ID: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" value="<?php print_r($consulta[0]->id) ?>" name="id" readonly>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="<?php print_r($consulta[0]->name) ?>" name="name">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">URL Imagen: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="<?php print_r($consulta[0]->eventImg) ?>" name="eventImg">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Lugar: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <select class="select2_single form-control" tabindex="-1" name="speakerNames">
              <?php 
              $consulta2 = $this->db->get('lugar');
              echo "<option value='". $consulta[0]->speakerNames . "'>" . $consulta[0]->speakerNames . "</option>";
              foreach ($consulta2 ->result() as $row2){
                echo "<option value='". $row2->name . "'>" . $row2->name . "</option>";
              }
            ?>

            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="date" class="form-control" value="<?php print_r($consulta[0]->fecha) ?>" name="fecha">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Hora de Inicio: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="time" class="form-control" value="<?php print_r($consulta[0]->timeStart) ?>" name="timeStart">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Hora de Fin: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="time" class="form-control" value="<?php print_r($consulta[0]->timeEnd) ?>" name="timeEnd">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción: <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <textarea class="form-control" rows="3" name="description"><?php print_r($consulta[0]->description) ?></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Link del Evento: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="<?php print_r($consulta[0]->linkEvento) ?>" name="linkEvento">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Tags</label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <select class="select2_single form-control" tabindex="-1" name="tracks" required>
              <?php echo "<option value='". $consulta[0]->tracks . "'>" . $consulta[0]->tracks . "</option>";?>
              <option value="Cultural">Cultural</option>
              <option value="Deportivo">Deportivo</option>
              <option value="Moda">Moda</option>
              <option value="Artístico">Artístico</option>
              <option value="Político">Político</option>
              <option value="Educativo">Educativo</option>
              <option value="Religión">Religión</option>
              <option value="Informática">Informática</option>
              <option value="Ocio">Ocio</option>
            </select><br>
          </div>
        </div>
        
        
        <div class="form-group">
          <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
            <input type="reset" class="btn btn-primary" value="Limpiar">
            <input type="submit" class="btn btn-success" value="Guardar">
          </div>
        </div>

      </form>
    </div>
  </div>
</div>
</div>