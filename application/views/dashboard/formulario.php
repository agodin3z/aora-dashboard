
<div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="Nombre del evento..." name="name" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">URL Imagen: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="Nombre del evento..." name="eventImg">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Lugar: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <select class="select2_single form-control" tabindex="-1" name="speakerNames" required>
              <?php 
              $consulta2 = $this->db->get('lugar');
              echo "<option value='". $row->speakerNames . "'>" . $row->speakerNames . "</option>";
              /*foreach ($consulta2 ->result() as $row2){
                echo "<option value='". $row2->name . "'>" . $row2->name . "</option>";
              }*/
            ?>

            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="date" class="form-control" placeholder="1/1/17" name="fecha" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Hora de Inicio: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="time" class="form-control" placeholder="00:00" name="timeStart" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Hora de Fin: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="time" class="form-control" placeholder="00:00" name="timeEnd" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción: <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <textarea class="form-control" rows="3" placeholder="Descripcion del evento..." name="description" required></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Link del Evento: </label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input type="text" class="form-control" placeholder="http://evento" name="linkEvento">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Tags</label>
          <div class="col-md-6 col-sm-9 col-xs-12">
            <input id="tags_1" type="text" class="tags form-control" placeholder="social, adverts, sales" name="tracks" required />
            <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
          </div>
        </div>
        
        
        <div class="form-group">
          <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
            <input type="reset" class="btn btn-primary" value="Limpiar">
            <input type="submit" class="btn btn-success" value="Guardar">
          </div>
        </div>