<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dashboard | AORA Apps</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= base_url('assets/vendors/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= base_url('assets/vendors/nprogress/nprogress.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>

    <!-- Custom Theme Style -->
    <link href="<?= base_url('assets/build/css/custom.min.css')?>" rel="stylesheet">
    <style>
      .site_title {
          padding-left: 15px;
      }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?= base_url()?>" class="site_title"> <img src="<?= base_url('assets/images/aora.png')?>" alt=""> <span>AORA Events</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_info" style="width: 75% !important;">
                <h2>Bienvenido, <?= $this->session->userdata('user_name'); ?></h2>
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Menú General</h3>
                <ul class="nav side-menu">
                  <li><a href="?p=home"><i class="fa fa-home"></i> Inicio </a></li>
                  <li><a><i class="fa fa-edit"></i> Formularios <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="?p=lugares">Registrar Lugares</a></li>
                      <li><a href="?p=eventos">Registrar Eventos</a></li>
                      <!--<li><a href="?p=modLugares">Modificar Lugares</a></li>
                      <li><a href="?p=modEventos">Modificar Eventos</a></li>-->
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> Datos <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="?p=verLugares">Lugares</a></li>
                      <li><a href="?p=verEventos">Eventos</a></li>
                    </ul>
                  </li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small" style="width: 230px">
              <!--<a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>-->
              <a data-toggle="tooltip" data-placement="top" title="Cerrar Sesión" href="<?= base_url('Login/user_logout');?>" style="width: 100%">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?= base_url('assets/images/img.jpg')?>" alt=""><?= $this->session->userdata('user_name'); ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <!--<li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>-->
                    <li><a href="<?= base_url('Login/user_logout');?>"><i class="fa fa-sign-out pull-right"></i> Cerrar Sesión</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
          <?php
            //$controller = $this->session->flashdata('controller');

            /*if(isset($_GET["p"]) && trim($_GET["p"]) == 'home'){
               $this->load->view('dashboard/home');
            }*/
            if(isset($_GET["p"]) && trim($_GET["p"]) == 'home'){
                $this->load->view('dashboard/home');
            }
            elseif(isset($_GET["p"]) && trim($_GET["p"]) == 'lugares'){
                $this->load->view('dashboard/lugares');
            }
            elseif(isset($_GET["p"]) && trim($_GET["p"]) == 'verEventos'){
                $this->load->view('dashboard/verEventos');
            }
            elseif(isset($_GET["p"]) && trim($_GET["p"]) == 'verLugares'){
                $this->load->view('dashboard/verLugares');
            }
            elseif(isset($_GET["p"]) && trim($_GET["p"]) == 'modEventos'){
                $this->load->view('dashboard/modEventos');
            }
            elseif(isset($_GET["p"]) && trim($_GET["p"]) == 'modLugares'){
                $this->load->view('dashboard/modLugares');
            }
            elseif(isset($_GET["p"]) && trim($_GET["p"]) == 'delEventos'){
                $this->load->view('dashboard/delEventos');
            }
            elseif(isset($_GET["p"]) && trim($_GET["p"]) == 'delLugares'){
                $this->load->view('dashboard/delLugares');
            }
            elseif(isset($_GET["p"]) && trim($_GET["p"]) == 'eventos'){
                $this->load->view('dashboard/eventos');
            } else {
              $this->load->view('dashboard/home');
            }
          ?>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            &copy; AORA Apps - Dashboard for Events App
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?= base_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>
    <!-- Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url('assets/vendors/fastclick/lib/fastclick.js')?>"></script>
    <!-- NProgress -->
    <script src="<?= base_url('assets/vendors/nprogress/nprogress.js')?>"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="<?= base_url('assets/build/js/custom.min.js')?>"></script>
  </body>
</html>