<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <title>AORA Apps</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="description" content="Expand, contract, animate forms with jQuery wihtout leaving the page" />
        <meta name="keywords" content="expand, form, css3, jquery, animate, width, height, adapt, unobtrusive javascript"/>
		<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/style.css')?>" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>
		<script src="<?= base_url('assets/js/cufon-yui.js')?>" type="text/javascript"></script>
		<script type="text/javascript">
			Cufon.replace('h1',{ textShadow: '1px 1px #fff'});
			Cufon.replace('h2',{ textShadow: '1px 1px #fff'});
			Cufon.replace('h3',{ textShadow: '1px 1px #000'});
			Cufon.replace('.back');
		</script>
		<style>
			input:disabled, input[type="submit"]:disabled { cursor: not-allowed; }
		</style>
    </head>
    <body>
		<div class="wrapper">
			<h1 align="center"><span style="color:#468fc7">AORA</span> <span style="color:#b359c8">Apps</span> <br /> 
			<span style="color:#3562ba">Panel de Administración</span></h1><br />
			<?php
              //$success_msg= $this->session->flashdata('success_msg');
              $error_msg= $this->session->flashdata('error_msg');
                  if($error_msg){
                    ?>
                    <script>
                    	swal('Oops...','Datos incorrectos!','error')
                    </script>
                    <?php
                  }
                  ?>
			<div class="content">
				<div id="form_wrapper" class="form_wrapper">
					<form class="register" role="form" action="<?= base_url('Login/register_user'); ?>" method="POST">
						<h3>Registrarse (Próximamente)</h3>
						<div class="column">
							<div>
								<label>Nombre:</label>
								<input type="text" disabled/>
								<span class="error">This is an error</span>
							</div>
							<div>
								<label>Email:</label>
								<input type="text" disabled/>
								<span class="error">This is an error</span>
							</div>
							<div>
								<label>Contraseña:</label>
								<input type="text" disabled name="user_name"/>
								<span class="error">This is an error</span>
							</div>
						</div>
						<div class="column">
							<div>
								<label>Apellido:</label>
								<input type="text" disabled/>
								<span class="error">This is an error</span>
							</div>
							<div>
								<label>Usuario:</label>
								<input type="text" disabled name="user_password"/>
								<span class="error">This is an error</span>
							</div>
							<div>
								<label>Confirmar contraseña:</label>
								<input type="password" disabled/>
								<span class="error">This is an error</span>
							</div>
						</div>
						<div class="bottom">
							<input type="submit" value="Registrarse" disabled />
							<a href="index.html" rel="login" class="linkform">Ya tienes una cuenta? Inicia sesión</a>
							<div class="clear"></div>
						</div>
					</form>
					<form class="login active" role="form" action="<?= base_url('Login/login_user'); ?>" method="POST" autocomplete="off">
						<h3>Iniciar Sesión</h3>
						<div>
							<label>Usuario:</label>
							<input type="text" name="user_name" required/>
							<span class="error">This is an error</span>
						</div>
						<div>
							<label>Contraseña: <!--<a href="forgot_password.html" rel="forgot_password" class="forgot linkform">Forgot your password?</a>--></label>
							<input type="password" name="user_password" required/>
							<span class="error">This is an error</span>
						</div>
						<div class="bottom">
							<input type="submit" value="Entrar"></input>
							<a href="register.html" rel="register" class="linkform">No tienes una cuenta? Regístrate</a>
							<div class="clear"></div>
						</div>
					</form>
					<form class="forgot_password">
						<h3>Olvid&eacute; la contraseña</h3>
						<div>
							<label>Usuario o Email:</label>
							<input type="text" />
							<span class="error">This is an error</span>
						</div>
						<div class="bottom">
							<input type="submit" value="Send reminder"></input>
							<a href="index.html" rel="login" class="linkform">Ya la recueras? Inicia sesión Aquí</a>
							<a href="register.html" rel="register" class="linkform">No tienes una cuenta? Regístrate</a>
							<div class="clear"></div>
						</div>
					</form>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		

		<!-- The JavaScript -->
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
		<script type="text/javascript">
			$(function() {
					//the form wrapper (includes all forms)
				var $form_wrapper	= $('#form_wrapper'),
					//the current form is the one with class active
					$currentForm	= $form_wrapper.children('form.active'),
					//the change form links
					$linkform		= $form_wrapper.find('.linkform');
						
				//get width and height of each form and store them for later						
				$form_wrapper.children('form').each(function(i){
					var $theForm	= $(this);
					//solve the inline display none problem when using fadeIn fadeOut
					if(!$theForm.hasClass('active'))
						$theForm.hide();
					$theForm.data({
						width	: $theForm.width(),
						height	: $theForm.height()
					});
				});
				
				//set width and height of wrapper (same of current form)
				setWrapperWidth();
				
				/*
				clicking a link (change form event) in the form
				makes the current form hide.
				The wrapper animates its width and height to the 
				width and height of the new current form.
				After the animation, the new form is shown
				*/
				$linkform.bind('click',function(e){
					var $link	= $(this);
					var target	= $link.attr('rel');
					$currentForm.fadeOut(400,function(){
						//remove class active from current form
						$currentForm.removeClass('active');
						//new current form
						$currentForm= $form_wrapper.children('form.'+target);
						//animate the wrapper
						$form_wrapper.stop()
									 .animate({
										width	: $currentForm.data('width') + 'px',
										height	: $currentForm.data('height') + 'px'
									 },500,function(){
										//new form gets class active
										$currentForm.addClass('active');
										//show the new form
										$currentForm.fadeIn(400);
									 });
					});
					e.preventDefault();
				});
				
				function setWrapperWidth(){
					$form_wrapper.css({
						width	: $currentForm.data('width') + 'px',
						height	: $currentForm.data('height') + 'px'
					});
				}
				
			});
        </script>
    </body>
</html>