<?php
  class UserModel extends CI_model{

    public function login_user($usernm,$pass) {
      $this->db->select('*');
      $this->db->from('user');
      $this->db->where('user_password',$pass);

      if($query=$this->db->get()) {
        return $query->row_array();
      }
      else {
        return false;
      }
    }

    public function user_check($user_name) {
      $this->db->select('*');
      $this->db->from('user');
      $this->db->where('user_name',$user_name);
      $query=$this->db->get();
      
      if($query->num_rows()>0) {
        return false;
      }
      else {
        return true;
      }
    }

    public function check_log($username,$passwd) {
      $this->db->select('*');
      $this->db->from('user');
      $this->db->where('user_name',$username);
      $this->db->where('user_password',$passwd);
      $query=$this->db->get();
      
      if($query->num_rows()>0) {
        return false;
      }
      else {
        return true;
      }
    }

    public function login_data() {
      $this->db->select('*');
      $this->db->from('user');
      $this->db->order_by('user_id');

      if($query=$this->db->get()) {
        return $query->result();
      }
      else {
        return false;
      }
    }

  }
?>