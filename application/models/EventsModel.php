<?php

  class EventsModel extends CI_model {

    public function register_event() {
        $notif = array();
        $data = array(
            'id' => NULL,
            'speakerNames' => $this->input->post('speakerNames'),
            'timeStart' => $this->input->post('timeStart'),
            'timeEnd' => $this->input->post('timeEnd'),
            'description' => $this->input->post('description'),
            'linkEvento' => $this->input->post('linkEvento'),
            'tracks' => $this->input->post('tracks'),
            'name' => $this->input->post('name'),
            'eventImg' => $this->input->post('eventImg')
            
        );
        $this->db->insert('evento', $data);
        //$users_id = $this->db->insert_id();
        if ($this->db->affected_rows() > 0) {
            $this->session->set_flashdata('success_msg', 'Registro guardado.');
            //$notif['message'] = 'Saved successfully';
            //$notif['type'] = 'success';
            unset($_POST);
        } else {
            //$notif['message'] = 'Something wrong !';
            //$notif['type'] = 'danger';
          $this->session->set_flashdata('error_msg', 'Error occured, Try again.');
        }
        //return $notif;
    }

    public function register_place() {
        $notif = array();
        $data = array(
            'id' => NULL,
            'profilePic' => $this->input->post('profilePic'),
            'twitter' => $this->input->post('twitter'),
            'about' => $this->input->post('about'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            'location' => $this->input->post('location'),
            'name' => $this->input->post('name'),
            'depto' => $this->input->post('depto'),
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng')
            
        );
        $this->db->insert('lugar', $data);
        //$users_id = $this->db->insert_id();
        if ($this->db->affected_rows() > 0) {
            $this->session->set_flashdata('success_msg', 'Registro guardado.');
            //$notif['message'] = 'Saved successfully';
            //$notif['type'] = 'success';
            unset($_POST);
        } else {
            //$notif['message'] = 'Something wrong !';
            //$notif['type'] = 'danger';
          $this->session->set_flashdata('error_msg', 'Error occured, Try again.');
        }
        //return $notif;
    }

    public function place_data() {
      $this->db->select('*');
      $this->db->from('lugar');
      $this->db->order_by('id');

      if($query=$this->db->get()) {
        return $query->result();
      }
      else {
        return false;
      }
    }

    public function event_data() {
      $this->db->select('*');
      $this->db->from('evento');
      $this->db->order_by('fecha');

      if($query=$this->db->get()) {
        return $query->result();
      }
      else {
        return false;
      }
    }

    //var $schedule;
    //var $speakers;
    //var $map;
    public function genJSON() {
     
      $i = 0;
      $j = 0;
      $h = 0;
      
      $datos = [];
      
      $datos = new \stdClass();
      //$datos = ;

      $this->db->select('fecha');
      $this->db->from('evento');
      $this->db->where('fecha >= ', date("Y-m-d", time()));
      $this->db->order_by('fecha','ASC');
      $this->db->group_by('fecha');
      $consulta = $this->db->get();
      //die(print_r(json_encode($consulta ->result())));      
      foreach ($consulta ->result() as $row){        
        
        $datos->schedule[$i]->groups[$j]->date = $row->fecha;

        $this->db->select('*');
        $this->db->from('evento');
        $this->db->where('fecha',$row->fecha);
        $this->db->order_by('timeStart','ASC');

        $consulta2 = $this->db->get();
        foreach ($consulta2 ->result() as $row2){
          //die(print_r($row2));
          if ($row->fecha == $row2->fecha) {
            $datos->schedule[$i]->groups[$j]->sessions[] = $row2;
          }
          
          //die(print_r(json_encode($datos)));
        }
        $j++;
      }      
      
      $this->db->select('*');
      $this->db->from('lugar');
      $consulta3 = $this->db->get();      
      foreach ($consulta3 ->result() as $row3){
        $datos->speakers[] = $row3;
        
        /*$this->db->select('*');
        $this->db->from('evento');
        $this->db->where('lugar',$row3->nomLugar);
        $this->db->order_by('fecha');
        $consulta4 = $this->db->get();
        foreach ($consulta4 ->result() as $row4){
          $datos->speakers->sessions[] = $row4;
        }*/
      }

      $this->db->select('name, lat, lng');
      $this->db->from('lugar');
      $consulta5 = $this->db->get();      
      foreach ($consulta5->result() as $row5){
        $datos->map[$h] = $row5;
        $datos->map[$h]->center = true;
        $h++;
      }      
    
      //$datos->map = array(8850,8900,8875);
     return $datos;
    }
  }

  
?>