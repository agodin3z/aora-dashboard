<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct(){
    parent::__construct();
		$this->load->helper('url');
    $this->load->library('session');
    $this->load->model('UserModel');
    //error_reporting(0);
	}

	public function index() {
		if (!$this->session->userdata('user_name')) {
      $this->session->sess_destroy();
      redirect('login');
    }
    else {            
      $this->load->view('dashboard');
      //$notif = '';
    }		
	}

  public function register_event() {
        //$notif = '';
    $nombre = $this->input->post('name');
    
        $data = array(
            'id' => NULL,
            'speakerNames' => $this->input->post('speakerNames'),
            'timeStart' => $this->input->post('timeStart'),
            'timeEnd' => $this->input->post('timeEnd'),
            'description' => $this->input->post('description'),
            'linkEvento' => $this->input->post('linkEvento'),
            'tracks' => $this->input->post('tracks'),
            'name' => $this->input->post('name'),
            'fecha' => $this->input->post('fecha'),
            'eventImg' => $this->input->post('eventImg')
            
        );
        $this->db->insert('evento', $data);
        //$users_id = $this->db->insert_id();
        if ($this->db->affected_rows() > 0) {
            //$notif['message'] = 'Saved successfully';
            //$notif = 'success';
            unset($_POST);

            $this->send_message('¡Nuevo evento: ',$nombre.'!');

            redirect("Dashboard/?p=eventos");
        } else {
            //$notif['message'] = 'Something wrong !';
            //$notif = 'danger';
          //$this->session->set_flashdata('error_msg', 'Error occured, Try again.');
          redirect("Dashboard/?p=eventos");
        }
        return $notif;
    }

    public function register_place() {
        //$notif = '';
        $nombre = $this->input->post('name');

        $data = array(
            'id' => NULL,
            'profilePic' => $this->input->post('profilePic'),
            'twitter' => $this->input->post('twitter'),
            'about' => $this->input->post('about'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            'location' => $this->input->post('location'),
            'name' => $this->input->post('name'),
            'depto' => $this->input->post('depto'),
            'lat' => $this->input->post('lat'),
            'lng' => $this->input->post('lng')
            
        );
        $this->db->insert('lugar', $data);
        //$users_id = $this->db->insert_id();
        if ($this->db->affected_rows() > 0) {
            //$this->session->set_flashdata('success_msg', 'Registro guardado.');
            //$notif['message'] = 'Saved successfully';
            //$notif = 'success';
            unset($_POST);

            $this->send_message('Un nuevo local para disfrutar: ',$nombre);

            redirect("Dashboard/?p=lugares");
        } else {
            //$notif['message'] = 'Something wrong !';
            //$notif = 'danger';
          //$this->session->set_flashdata('error_msg', 'Error occured, Try again.');
          redirect("Dashboard/?p=lugares");
        }
        return $notif;
    }

    public function change_event($id)
    {
        $_SESSION['id'] = $id;
        redirect("Dashboard/?p=modEventos");
    }

    public function change_place($id)
    {
        $_SESSION['id'] = $id;
        redirect("Dashboard/?p=modLugares");
    }

    public function erase_event($id)
    {
        $_SESSION['id'] = $id;
        redirect("Dashboard/?p=delEventos");
    }

    public function erase_place($id)
    {
        $_SESSION['id'] = $id;
        redirect("Dashboard/?p=delLugares");
    }

    public function update_place() {
        //$notif = '';
        $nombre = $this->input->post('name');

        if (strlen($this->input->post('name')) > 0){
                $this->db->select('name');
                $this->db->from('lugar');
                $this->db->where('id',$this->input->post('id'));
                $anterior = $this->db->get()->result();
                
                $this->db->set('speakerNames',$this->input->post('name'));
                $this->db->where('speakerNames', $anterior[0]->name);
                $this->db->update('evento');             
            }
            
        
        if (strlen($this->input->post('profilePic')) > 0){
         $this->db->set('profilePic',$this->input->post('profilePic'));
        }
        if (strlen($this->input->post('twitter')) > 0){
         $this->db->set('twitter',$this->input->post('twitter'));
        }
        if (strlen($this->input->post('about')) > 0){
         $this->db->set('about',$this->input->post('about'));
        }
        if (strlen($this->input->post('email')) > 0){
         $this->db->set('email',$this->input->post('email'));
        }
        if (strlen($this->input->post('phone')) > 0){
         $this->db->set('phone',$this->input->post('phone'));
        }
        if (strlen($this->input->post('location')) > 0){
         $this->db->set('location',$this->input->post('location'));
        }
        if (strlen($this->input->post('name')) > 0){
         $this->db->set('name',$this->input->post('name'));
        }
        if (strlen($this->input->post('depto')) > 0){
         $this->db->set('depto',$this->input->post('depto'));
        }
        if (strlen($this->input->post('lat')) > 0){
         $this->db->set('lat',$this->input->post('lat'));
        }
        if (strlen($this->input->post('lng')) > 0){
         $this->db->set('lng',$this->input->post('lng'));
        }
            
        $this->db->where('id', $this->input->post(('id')));
        $this->db->update('lugar');
        //$users_id = $this->db->insert_id();
        if ($this->db->affected_rows() > 0) {
            //$this->session->set_flashdata('success_msg', 'Registro guardado.');
            //$notif['message'] = 'Saved successfully';
            //$notif = 'success';
            unset($_POST);

            $this->send_message('Actualizamos la info del local: ',$nombre);

            redirect("Dashboard/?p=verLugares");
        } else {
            //$notif['message'] = 'Something wrong !';
            //$notif = 'danger';
          //$this->session->set_flashdata('error_msg', 'Error occured, Try again.');
          redirect("Dashboard/?p=verLugares");
        }
        return $notif;
    }

    public function update_event() {
        //$notif = '';
        //die(print_r($this->input->post('id')));
        $nombre = $this->input->post('name');

        if (strlen($this->input->post('speakerNames')) > 0){
         $this->db->set('speakerNames',$this->input->post('speakerNames'));
        }
        if (strlen($this->input->post('timeStart')) > 0){
         $this->db->set('timeStart',$this->input->post('timeStart'));
        }
        if (strlen($this->input->post('timeEnd')) > 0){
         $this->db->set('timeEnd',$this->input->post('timeEnd'));
        }
        if (strlen($this->input->post('description')) > 0){
         $this->db->set('description',$this->input->post('description'));
        }
        if (strlen($this->input->post('linkEvento')) > 0){
         $this->db->set('linkEvento',$this->input->post('linkEvento'));
        }
        if (strlen($this->input->post('tracks')) > 0){
         $this->db->set('tracks',$this->input->post('tracks'));
        }
        if (strlen($this->input->post('name')) > 0){
         $this->db->set('name',$this->input->post('name'));
        }
        if (strlen($this->input->post('fecha')) > 0){
         $this->db->set('fecha',$this->input->post('fecha'));
        }
        if (strlen($this->input->post('eventImg')) > 0){
         $this->db->set('eventImg',$this->input->post('eventImg'));
        }

        
        //$this->db->set($data);
        $this->db->where('id', $this->input->post('id'));
        //$sql = $this->db->get_compiled_update('parcel_r‌​equests');
        //die(print_r($sql));
        $this->db->update('evento');
        //$users_id = $this->db->insert_id();
        if ($this->db->affected_rows() > 0) {
            //$this->session->set_flashdata('success_msg', 'Registro guardado.');
            //$notif['message'] = 'Saved successfully';
            //$notif = 'success';
            unset($_POST);

            $this->send_message('Actualizamos la info del evento: ',$nombre);

            redirect("Dashboard/?p=verEventos");
        } else {
            //$notif['message'] = 'Something wrong !';
            //$notif = 'danger';
          //$this->session->set_flashdata('error_msg', 'Error occured, Try again.');

          //redirect("Dashboard/?p=verEventos");
            redirect("Dashboard/?p=verEventos");
        }
        //return $notif;
    }

    public function delete_place()
    {
        $nombre = $this->input->post('name');

        $this->db->where('id', $this->input->post('id'));
        $this->db->delete('lugar');

        if ($this->db->affected_rows() > 0) {
            //$this->session->set_flashdata('success_msg', 'Registro guardado.');
            //$notif['message'] = 'Saved successfully';
            //$notif = 'success';
            unset($_POST);

            $this->send_message('Hemos borrado el local: ',$nombre);

            redirect("Dashboard/?p=verLugares");
        } else {
            //$notif['message'] = 'Something wrong !';
            //$notif = 'danger';
          //$this->session->set_flashdata('error_msg', 'Error occured, Try again.');

          //redirect("Dashboard/?p=verEventos");
            redirect("Dashboard/?p=verLugares");
        }
    }

    public function delete_event()
    {
            $nombre = $this->input->post('name');
            
            $this->db->where('id', $this->input->post('id'));
            $this->db->delete('evento');

            
            if ($this->db->affected_rows() > 0) {
                //$this->session->set_flashdata('success_msg', 'Registro guardado.');
                //$notif['message'] = 'Saved successfully';
                //$notif = 'success';
                unset($_POST);

                $this->send_message('Se cancela el evento: ',$nombre);

                redirect("Dashboard/?p=verEventos");
            } else {
                //$notif['message'] = 'Something wrong !';
                //$notif = 'danger';
              //$this->session->set_flashdata('error_msg', 'Error occured, Try again.');

              //redirect("Dashboard/?p=verEventos");
                redirect("Dashboard/?p=verEventos");
            }
    }


    public function send_message($mensaje,$nombre){
        $message = $mensaje . $nombre;
        $content = array(
            "en" => "$message"
        );

        $fields = array(
            'app_id' => "0e6c0cd2-887f-44ff-b26c-b1e927f04ad3",
            'included_segments' => array('All'),
            'data' => array("foo" => "bar"),
            'contents' => $content
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic Y2UyYjc3ZDUtZjY5MS00MjYzLTg1NWYtMzkxMjcxNjUzYmNh'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
       return $response;
    }

  }

?>