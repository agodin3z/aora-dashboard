<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InfoPlace extends CI_Controller {
	public function __construct(){
    parent::__construct();
		$this->load->helper('url');
		$this->load->model('EventsModel');
    $this->load->library('session');
    error_reporting(0);
	}

	public function index() {
		//$raw=$this->EventsModel->place_data();
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
  	//echo json_encode($raw);
  	print_r(json_encode($this->EventsModel->genJSON(),JSON_NUMERIC_CHECK));
	}
}