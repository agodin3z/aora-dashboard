<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
    parent::__construct();
		$this->load->helper('url');
 		$this->load->model('UserModel');
    $this->load->library('session');
	}


	public function index() {
		if (!$this->session->userdata('user_name')) {
      $this->session->sess_destroy();
		 	$this->load->view('login');
		} else {
			redirect('dashboard');
		}
	}

	function login_user(){
	  $user_login=array(
	  	'user_name'=>$this->input->post('user_name'),
	  	'user_password'=>md5($this->input->post('user_password'))
	  );

	  $data=$this->UserModel->check_log($user_login['user_name'],$user_login['user_password']);
	  //die(print_r($user_login['user_name'], true));
	  if($data > 0)  {
      //die("no existe" . " " . $data . " " . $user_login['user_name']);
      $this->session->set_flashdata('error_msg', 'Error occured,Try again.');
      redirect('');
    }
	  else{
	  	//session_start();
	  	//$_SESSION['user_name'] = print_r($user_login['user_name'], true);
	    $this->session->set_userdata('user_name',print_r($user_login['user_name'], true));
	    //$_SESSION['user']=$user_login['user_name'];
	    //$this->load->view("dashboard/home");
	    redirect('dashboard');
	    //die("existe" . " " . $data . " " . $user_login['user_name']);
	  }
	}

	/*public function register_view() {
		$this->load->view("register");
	}


	public function register_user(){
    $user=array(
    	'user_name'=>$this->input->post('user_name'),
    	'user_password'=>md5($this->input->post('user_password')),
    );
    print_r($user);

		$user_check=$this->UserModel->user_check($user['user_name']);

		if($user_check){
		  $this->UserModel->register_user($user);
		  $this->session->set_flashdata('success_msg', 'Registered successfully.Now login to your account.');
		  redirect('User');
		}
		else{
		  $this->session->set_flashdata('error_msg', 'Error occured,Try again.');
		  redirect('User/register_view');
		}
	}*/

	function user_profile(){
		$this->load->view('user_profile');
	}

	public function user_logout(){
  	$this->session->sess_destroy();
  	//session_destroy();
  	redirect('/', 'refresh');
	}


}
